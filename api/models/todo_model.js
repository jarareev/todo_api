var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model
var TodoSchema = new Schema({
	description: {
		type: String,
		unique: true,
		required: true
	},
	status: {
		type: String,
		required: true
	},
	createdAt: { type: Date, default: Date.now },
	updatedAt: { type: Date, default: Date.now }
});


module.exports = mongoose.model('Todo', TodoSchema);
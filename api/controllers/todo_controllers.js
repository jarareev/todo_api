var express = require("express");
var routes = express.Router();
var Todo = require("../models/todo_model");

routes.post("/create", (req, res) => {
  var newTodo = new Todo({
    description: req.body.desc,
    status: req.body.status
  });
  newTodo.save(function(err, todo) {
    if (err) return res.status(500).send(err);

    res.status(200).send({ todo });
  });
});

routes.post("/view", (req, res) => {
  Todo.find({ description: req.body.desc }, function(err, todo) {
    if (err) return res.status(500).send(err);

    return res.json({
      success: true,
      response: todo
    });
  });
});

routes.delete("/delete", (req, res) => {
  console.log(req.body);
  Todo.findOneAndRemove({ description: req.body.desc }, function(err, todo) {
    if (err) return res.status(500).send(err);

    return res.json({
      success: true,
      response: "Deleted"
    });
  });
});

routes.put("/edit", (req, res) => {
  console.log(req.body);
  Todo.findOneAndUpdate(
    {
      description: req.body.desc
    },
    {
      status: req.body.status
    },
    {
      new: true,
      runValidators: true
    }
  )
    .then(todo => {
      return res.json({
        success: true,
        response: "Updated"
      });
    })
    .catch(err => {
      return res.status(500).send(err);
    });
});

// FILTER OPERATIONS
routes.get("/status", (req, res) => {
  Todo.find({ status: req.headers.status })
    .then(todo => {
      return res.json({ success: true, response: todo });
    })
    .catch(err => {
      return res.status(500).send(err);
    });
});

module.exports = routes;

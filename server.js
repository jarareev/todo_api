const express=require('express');
const bodyParser = require('body-parser');
const morgan=require("morgan");
var mongoose = require('mongoose');
var config = require('./config/database'); // get db config file

const app=express();
const port = process.env.PORT || 8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

// log to console
app.use(morgan('dev'));

mongoose.connect(config.database, { useNewUrlParser: true });

// connect the api routes under /api/*
app.use('/api', require('./api/controllers/todo_controllers'));

app.listen(port,(err)=>{
	if(!err){
		console.log("Listening....")
	}
})